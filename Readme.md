# Application monolithique en React

Cette application a été crée pour avoir un exemple d'application SPA Web monolithique sur lequel travailler.
Elle a été crée avec Create-React-App

Les principaux outils sont 
- React
- Carbon Design system
- react-router (routing, loader, action)
- L'API local storage

# Qu'est-ce-qu'elle fait

C'est une application qui permet de créer des produits d'assurance et de suivre leurs rentabilité dans le temps.
La prod est [ici](https://cop-spa-monolith.vercel.app/)

# Git

Ce projet est un sous-modules du repo cop-micro-frontend
On s'inspire de [conventional commit](https://www.conventionalcommits.org/en/v1.0.0/#summary) pour les standards de commits

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
