import {Header, HeaderMenuItem, HeaderName, HeaderNavigation} from "@carbon/react";
import React from "react";
import {Link} from "react-router-dom";

export const Nav = () => {
    return (
        <Header>
            <HeaderName element={Link} to="/presentation-valeur" prefix="[Micro]">
                Assu
            </HeaderName>
            <HeaderNavigation>
                <HeaderMenuItem element={Link} to="/presentation-valeur">Notre entreprise</HeaderMenuItem>
                <HeaderMenuItem element={Link} to="/creation-projet">Créer un projet</HeaderMenuItem>
                <HeaderMenuItem element={Link} to="/suivi-projet">Suivre vos projet</HeaderMenuItem>
                <HeaderMenuItem element={Link} to="/suivi-produit">Suivre vos produits</HeaderMenuItem>
            </HeaderNavigation>
        </Header>
    )
}