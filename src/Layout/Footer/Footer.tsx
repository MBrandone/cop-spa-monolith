export const Footer = () => {
    return (
        <footer>
            <p>Auteur: Micro-Frontend-Team@OCTO<br/>
                <a href="mailto:octo.software-engineering.microfrontend@accenture.com">Contactez-nous !</a></p>
        </footer>)
}