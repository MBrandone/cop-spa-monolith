import React from "react";
import {Outlet} from "react-router-dom";
import {Footer} from "./Footer/Footer";
import {Nav} from "./Nav/Nav";
import {Content} from "@carbon/react";

export const Layout = () => {
    return (
        <>
            <Nav/>
            <Content>
              <Outlet/>
            </Content>
            <Footer/>
        </>
    )
}