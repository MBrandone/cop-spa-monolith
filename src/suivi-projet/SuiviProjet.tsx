import styles from './SuiviProjet.module.css'
import {NavLink, Route, Routes, useRouteLoaderData} from "react-router-dom";
import React from "react";
import { ListeTachesProjet } from './ListeTachesProjet';
import {Projet} from "./infrastructure/liste-projets";


export const SuiviProjet = () => {
    const listeProjets: Projet[] = useRouteLoaderData('suivi-projet') as Projet[]

    return (
        <div className={styles.suiviProjetContent}>
            <h1>
                Suivi de vos projet
            </h1>
            <div>
                <h2>La liste des projets</h2>
                <ul>
                    {listeProjets.map((projet) => <li><NavLink to={`/suivi-projet/${projet.slug}`}>{projet.nom}</NavLink></li>)}
                </ul>
            </div>

                <Routes>
                    <Route path=":slug" element={<ListeTachesProjet/>}/>
                </Routes>

        </div>
    )
}
