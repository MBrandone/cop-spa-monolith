import {ProjetReadModel} from "./projet-readmodel";

export class RecupérerLaListeProjets {
    private projetReadModel: ProjetReadModel;

    constructor(projetReadModel: ProjetReadModel) {
        this.projetReadModel= projetReadModel
    }

    async éxécuter() {
        return await this.projetReadModel.récupérerTout()
    }
}
