import {Projet} from "../infrastructure/liste-projets";

export interface ProjetReadModel {
    récupérerTout: () => Promise<Projet[]>
}
