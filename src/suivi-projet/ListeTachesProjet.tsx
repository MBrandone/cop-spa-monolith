import {useParams} from "react-router-dom";
import React from "react";
import {ProgressIndicator, ProgressStep} from "@carbon/react";
import {listeProjets, STATUT_TACHE} from "./infrastructure/liste-projets";

export const ListeTachesProjet = () => {
    const { slug } = useParams()
    const projet = listeProjets.find((projet) => projet.slug === slug) || { nom: 'non trouvé', taches: []}

    return (
        <div>
            <h2>La liste des tâches pour le projet : {projet.nom}</h2>
            <ProgressIndicator>
                {projet.taches.map((tache) => {
                    return (
                        <ProgressStep
                            key={tache.nom}
                            label={tache.nom}
                            secondaryLabel={tache.description}
                            complete={tache.statut === STATUT_TACHE.FAIT}
                            current={tache.statut === STATUT_TACHE.EN_COURS}
                            invalid={tache.statut === STATUT_TACHE.A_FAIRE}
                        />
                    )

                })}
            </ProgressIndicator>
        </div>
    )
}
