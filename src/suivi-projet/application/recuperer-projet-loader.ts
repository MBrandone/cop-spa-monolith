import {RecupérerLaListeProjets} from "../domaine/récupérer-la-liste-projets";
import {LocalStorageProjetReadmodel} from "../infrastructure/local-storage-projet-readmodel";

export async function récupérerProjetLoader() {
    return await new RecupérerLaListeProjets(new LocalStorageProjetReadmodel()).éxécuter();
}
