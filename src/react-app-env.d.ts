/// <reference types="react-scripts" />
import {FC} from "react";

// @DEVNOTE: ajout support minimal typescript pour composants carbon manquants
declare module "@carbon/react" {
  declare const Button: FC<any>;
  declare const Form: FC<any>;
  declare const FormGroup: FC<any>;
  declare const Stack: FC<any>;
  declare const Header: FC<any>;
  declare const HeaderName: FC<any>;
  declare const HeaderNavigation: FC<any>;
  declare const HeaderMenuItem: FC<any>;
  declare const Content: FC<any>;
  declare const Tile: FC<any>;
  declare const ProgressIndicator: FC<any>;
  declare const ProgressStep: FC<any>;
  declare const Heading: FC<any>;
  declare const Section: FC<any>;
}


declare module "@carbon/icons-react" {
  declare const Product: FC<any>;
  declare const PenFountain: FC<any>;
  declare const Favorite: FC<any>;
  declare const IbmWatsonAssistant: FC<any>;
}