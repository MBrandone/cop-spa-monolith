import {Garantie} from '../domaine/Projet';
import {LoaderFunction, redirect} from 'react-router-dom';
import {CreationProjetCommande, CreerUnProjet} from '../domaine/creer-un-projet';
import {LocalStorageProjetRepository} from '../infrastructure/local-storage-projet-repository';

function transformerEnCreationProjetCommande(formData: FormData): CreationProjetCommande {
    const nameFormEntry = formData.get('name');
    const nom = nameFormEntry ? nameFormEntry.toString() : '';

    const ambitionFormEntry = formData.get('message');
    const ambition = ambitionFormEntry ? ambitionFormEntry.toString() : '';

    const categorieFormEntry = formData.get('categorie');
    const categorie = categorieFormEntry ? categorieFormEntry.toString() : '';

    const garanties: Garantie[] = [Garantie.caution, Garantie.commerciale, Garantie["tout-risque"], Garantie.hypothèque, Garantie.nantissement]
        .filter(key => formData.get(key.toString()) != null);

    return {
        nom,
        ambition,
        categorie,
        garanties,
    };
}

export const creerProjetAction: LoaderFunction = async ({params, request}) => {
    const projetCommande: CreationProjetCommande = transformerEnCreationProjetCommande(await request.formData());
    await new CreerUnProjet(new LocalStorageProjetRepository()).executer(projetCommande);
    return redirect('/suivi-projet');
}