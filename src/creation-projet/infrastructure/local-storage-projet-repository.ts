import {ProjetRepository} from '../domaine/projet-repository';
import {Projet} from '../domaine/Projet';

export class LocalStorageProjetRepository implements ProjetRepository {
    private readonly storageKey = 'projets';

    enregistrer(projet: Projet): Promise<void> {
        localStorage.setItem(this.storageKey, JSON.stringify(projet));
        return Promise.resolve(undefined);
    }

    recupererTout(): Promise<Projet[]> {
        const presentStorage = localStorage.getItem(this.storageKey);
        if (presentStorage) {
            return Promise.resolve([JSON.parse(presentStorage)]);
        } else {
            return Promise.resolve([]);
        }
    }

}
