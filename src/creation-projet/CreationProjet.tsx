import TextInput from '@carbon/react/es/components/TextInput/TextInput';
import TextArea from '@carbon/react/lib/components/TextArea/TextArea';
import Select from '@carbon/react/lib/components/Select/Select';
import SelectItem from '@carbon/react/lib/components/SelectItem/SelectItem';
import {Button, Checkbox, Column, FormGroup, Grid, Stack} from '@carbon/react';
import {Form} from 'react-router-dom';

export const CreationProjet = () => {
    return (
        <Grid>
            <Column span={4}/>
            <Column span={8}>
                <h1>Créer un projet</h1>
                <Form method="post" action="/creation-projet">
                    <Stack gap={6}>
                        <TextInput type='text' id="name" name="name" labelText="Le nom du projet"
                                   autoComplete="name"/>
                        <TextArea id='message' name="message" labelText="Votre ambition"/>
                        <Select id="categorie" name="categorie" labelText="Catégories">
                            <SelectItem text="Nouveaux véhicules" value="nouveau-vehicule"/>
                            <SelectItem text="Retard de vol" value="retard-de-vol"/>
                            <SelectItem text="Habitation" value="habitation"/>
                            <SelectItem text="Assurance Vie" value="assurance-vie"/>
                        </Select>

                        <FormGroup role='group'
                                   legendText="Quelles garanties souhaitez-vous ? (Choisissez toutes celles qui vous intéressent.)">
                            <Checkbox name='tout-risque' id='tout-risque' labelText="Tout risque" invalidText="" warnText=""/>
                            <Checkbox name='caution' id='caution' labelText="Caution" invalidText="" warnText=""/>
                            <Checkbox name='commerciale' id='commerciale' labelText="Commerciale" invalidText="" warnText=""/>
                            <Checkbox name='nantissement' id='nantissement' labelText="Nantissement" invalidText="" warnText=""/>
                            <Checkbox name='hypothèque' id='hypothèque' labelText="Hypothèque" invalidText="" warnText=""/>
                        </FormGroup>

                        <Button type="submit">Créer mon projet</Button>
                    </Stack>
                </Form>
            </Column>
            <Column span={4}/>
        </Grid>
    );
}
