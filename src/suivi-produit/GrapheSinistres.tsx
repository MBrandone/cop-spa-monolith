import {Area, AreaChart, CartesianGrid, Tooltip, XAxis, YAxis} from "recharts";
import React from "react";

const data = [
    {
        "name": "Jan",
        "uv": 1000,
        "amt": 2400
    },
    {
        "name": "Fev",
        "uv": 1500,
        "amt": 2210
    },
    {
        "name": "Mars",
        "uv": 1550,
        "amt": 2290
    },
    {
        "name": "Avril",
        "uv": 1600,
        "amt": 2000
    },
    {
        "name": "Mai",
        "uv": 2000,
        "amt": 2181
    },
    {
        "name": "Juin",
        "uv": 4000,
        "amt": 2500
    },
    {
        "name": "Juil",
        "uv": 4500,
        "amt": 2100
    },
    {
        "name": "Aout",
        "uv": 4600,
        "amt": 2290
    },
    {
        "name": "Sept",
        "uv": 4650,
        "amt": 2000
    },
    {
        "name": "Oct",
        "uv": 4700,
        "amt": 2181
    },
    {
        "name": "Nov",
        "uv": 4720,
        "amt": 2500
    },
    {
        "name": "Déc",
        "uv": 4500,
        "amt": 2100
    }
]


export const GrapheSinistres = () => {
    return (<AreaChart width={730} height={250} data={data}
                       margin={{ top: 10, right: 30, left: 0, bottom: 0 }}>
        <defs>
            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
            </linearGradient>
            <linearGradient id="colorPv" x1="0" y1="0" x2="0" y2="1">
                <stop offset="5%" stopColor="#82ca9d" stopOpacity={0.8}/>
                <stop offset="95%" stopColor="#82ca9d" stopOpacity={0}/>
            </linearGradient>
        </defs>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid strokeDasharray="3 3" />
        <Tooltip />
        <Area type="monotone" dataKey="uv" stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)" />
    </AreaChart>)
}