import React from "react";
import { GrapheRentabilité } from "./GrapheRentabilité";
import { GrapheSinistres } from "./GrapheSinistres";
import {Heading, Stack} from "@carbon/react";
import styles from './SuiviProduit.module.css'


interface Produit {
    nom: string
}

const produit: Produit = {
    nom: 'Assurance tout risques'
}

export const SuiviProduit = () => {
    return (
        <div className={styles.suiviProduit}>
            <Heading>
                Suivi du produit {produit.nom}
            </Heading>
            <Stack gap={8}>
                <h2>
                    Graphe de rentabilité
                </h2>

                <GrapheRentabilité/>

                <div>Votre rentabilité peut-être optimisée. Pour suivre la procédure d'optimisation, appelez votre chef de projet.</div>
            </Stack>
            <Stack gap={8}>
                <h2>Graphe de sinistre</h2>
                <GrapheSinistres/>
            </Stack>
        </div>
    )
}

