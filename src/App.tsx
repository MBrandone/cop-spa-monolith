import React from 'react';
import {createBrowserRouter, RouterProvider} from 'react-router-dom';
import {PresentationValeur} from "./presentation-valeur/PresentationValeur";
import {CreationProjet} from "./creation-projet/CreationProjet";
import {SuiviProduit} from "./suivi-produit/SuiviProduit";
import {SuiviProjet} from "./suivi-projet/SuiviProjet";
import {Layout} from "./Layout/Layout";
import {récupérerProjetLoader} from "./suivi-projet/application/recuperer-projet-loader";
import {creerProjetAction} from "./creation-projet/application/creer-projet-action";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout/>,
        children: [
            {
                path: "",
                element: <PresentationValeur/>,
            },
            {
                path: "presentation-valeur",
                element: <PresentationValeur/>,
            },
            {
                path: "creation-projet",
                element: <CreationProjet/>,
                action: creerProjetAction,
            },
            {
                path: "suivi-produit",
                element: <SuiviProduit/>,
            },
            {
                path: "suivi-projet/*",
                element: <SuiviProjet/>,
                id: "suivi-projet",
                loader: récupérerProjetLoader
            },

        ]
    },
]);

function App() {
    return (<RouterProvider router={router}/>);
}

export default App;
