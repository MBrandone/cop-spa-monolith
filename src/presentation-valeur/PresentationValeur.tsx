import styles from './PresentationValeur.module.css'
import {Button, Column, Grid, Heading, Stack, Tile} from "@carbon/react";
// @ts-ignore
import {IbmWatsonAssistant, Product, Favorite, PenFountain} from "@carbon/icons-react"
import {NavLink} from "react-router-dom";

export const PresentationValeur = () => {
    return (<>
        <div className={styles.header}>
            <div className={styles.headings}>
                <Heading>Les produits sur-mesure ça ne s'invente pas</Heading>
                <Heading>Commencez avec nous aujourd'hui.</Heading>
            </div>
            <NavLink to="/creation-projet">
                <Button>
                    Créer un produit sur-mesure
                </Button>
            </NavLink>
        </div>

        <Stack as="section" gap={8}>
            <Heading>À propos de notre entreprise</Heading>
            <Grid>
                <Column sm={4}>
                    <Tile>
                        <Product size={32} />
                        <h3>Une grande gamme de produits</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore.</p>
                    </Tile>
                </Column>
                <Column sm={4}>
                    <Tile>
                        <Favorite size={32} />
                        <h3>Une équipe de passionés</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore.</p>
                    </Tile>
                </Column>
                <Column sm={4}>
                    <Tile>
                        <IbmWatsonAssistant size={32} />
                        <h3>Une relation de qualité</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore.</p>
                    </Tile>
                </Column>
                <Column sm={4}>
                    <Tile>
                        <PenFountain size={32} />
                        <h3>Du sur-mesure en temps et en heure</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore.</p>
                    </Tile>
                </Column>
            </Grid>
        </Stack>
    </>)
}